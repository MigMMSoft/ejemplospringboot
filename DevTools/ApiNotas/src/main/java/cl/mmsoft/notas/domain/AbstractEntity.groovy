package cl.mmsoft.notas.domain

import org.springframework.data.jpa.domain.support.AuditingEntityListener
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import javax.persistence.Column
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass
import javax.persistence.Temporal
import javax.persistence.TemporalType

/**
 *
 * @author mgonzalez@mmsoft.cl
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
abstract class AbstractEntity<U> {
    @Column(nullable = true, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    Date fechaCreacion
    
    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    Date fechaActualizacion
}

