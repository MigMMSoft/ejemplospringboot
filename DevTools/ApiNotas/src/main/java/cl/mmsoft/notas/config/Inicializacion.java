package cl.mmsoft.notas.config;

import cl.mmsoft.notas.servicio.NotaRepository;
import cl.mmsoft.notas.domain.EntidadNota;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author mgonzalez@mmsoft.cl
 */
@Configuration
@EntityScan(basePackages = {"cl.mmsoft.notas.domain"})
@EnableJpaRepositories("cl.mmsoft.notas.servicio")
public class Inicializacion implements ApplicationRunner{
    
    @Autowired
    private NotaRepository notaRepository;

    @Override
    public void run(ApplicationArguments aa) throws Exception {
        if(notaRepository.count()==0){
            EntidadNota nota1 = new EntidadNota();
            nota1.setTitulo("Nota 1");
            nota1.setComentario("Esta es la primera nota");
            nota1.setFechaCreacion(new Date());
            notaRepository.save(nota1);
        }
    }    
}
