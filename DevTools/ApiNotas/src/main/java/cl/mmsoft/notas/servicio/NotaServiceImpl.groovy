package cl.mmsoft.notas.servicio

import cl.mmsoft.notas.domain.EntidadNota
import cl.mmsoft.notas.swagger.codegen.model.Nota
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.data.domain.Sort;
import java.time.OffsetDateTime
import java.time.ZoneId

/**
 *
 * @author mgonzalez@mmsoft.cl
 */
@Service
@Transactional(readOnly = true)
class NotaServiceImpl implements Serializable{
    @Autowired
    private NotaRepository notaRepository;
    
    Nota getNota(notaId){
        EntidadNota entidadNota = notaRepository.getOne(notaId)
        if(entidadNota){
            getNotaSwagger(entidadNota)
        }else{
            null
        }
    }
    
    @Transactional
    void saveNota(nota){
        EntidadNota entidadNota = new EntidadNota(
            titulo:nota.titulo,
            comentario:nota.comentario)
        notaRepository.saveAndFlush(entidadNota)
    }
    
    @Transactional
    void update(nota){
        EntidadNota entidadNota = notaRepository.getOne(nota.id)
        if(entidadNota){
            entidadNota.titulo = nota.titulo
            entidadNota.comentario = nota.comentario
            notaRepository.saveAndFlush(entidadNota)
        }
    }
    
    @Transactional
    void delete(notaId){
        EntidadNota entidadNota = notaRepository.getOne(notaId)
        if(entidadNota){
            notaRepository.deleteInBatch([entidadNota])
        }
    }
    
    List<Nota> getNotas(Integer offset, Integer max){
        def entidadesNotas = 
            notaRepository.findAllNotas(new PageRequest( offset?:0, max?:10, Sort.Direction.DESC, "id" ))
        def notas = []
        entidadesNotas.each(){entidadNota->
            notas << getNotaSwagger(entidadNota)
        }
        notas
    }
    
    def getNotaSwagger(entidadNota){
        def nota = new Nota()
        nota.with {
            id = entidadNota.id
            titulo = entidadNota.titulo
            comentario = entidadNota.comentario 
        }
        if(entidadNota.fechaCreacion){
            nota.fechaCreacion = OffsetDateTime.ofInstant(entidadNota?.fechaCreacion?.toInstant(), ZoneId.systemDefault())
        }
        if(entidadNota.fechaActualizacion){
           nota.fechaActualizacion = OffsetDateTime.ofInstant(entidadNota?.fechaActualizacion?.toInstant(), ZoneId.systemDefault()) 
        }
        nota
    }
}

