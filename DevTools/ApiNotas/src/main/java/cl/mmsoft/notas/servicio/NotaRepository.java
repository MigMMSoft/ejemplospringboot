package cl.mmsoft.notas.servicio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import cl.mmsoft.notas.domain.EntidadNota;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author mig_s
 */
@Repository
public interface NotaRepository extends JpaRepository<EntidadNota, Long>{
    @Query( "select f from EntidadNota f" )
    Page<EntidadNota> findAllNotas(Pageable pageable);  
}
