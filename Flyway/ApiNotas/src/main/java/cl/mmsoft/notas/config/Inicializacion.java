package cl.mmsoft.notas.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author mgonzalez@mmsoft.cl
 */
@Configuration
@EntityScan(basePackages = {"cl.mmsoft.notas.domain"})
@EnableJpaRepositories("cl.mmsoft.notas.servicio")
public class Inicializacion {
      
}
