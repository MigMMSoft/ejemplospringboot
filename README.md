# EjemploSpringBoot

Ejemplo de implementación de Spring Boot utilizando Swagger

01 \SwaggerEjemplo
	- Ejemplo de generación de API con Swagger desde 0 para lenguaje Java.

02 \SwaggerPythonEjemplo
	- Ejemplo de generación de API con Swagger desde 0 para lenguaje Python.

03 \DevTools
	- Ejemplo de API con complemento de Spring DevTools

04 \JPA
	- Ejemplo de API con generación de base de datos desde un script utilizando spring JPA

05 \Flyway
	- Ejemplo de API con generación de base de datos utilizando Flyway

06 \Liquibase
	- Ejemplo de API con generación de base de datos utilizando Liquibase

07 \JOOQ
	- Ejemplo de API con generacion de base de datos utilizando JOOQ

08 \CoberturaTestEjemplo
	- Ejemplo de API con JUnit y reporte de covertura utilizando cobertura

09 \CoberturaSpockTestEjemplo
	- Ejemplo de API con Test Spock y reporte de covertura utilizando cobertura

Todas las instrucciones de ejecución de los proyectos en los readme.md respectivos de cada proyecto.
