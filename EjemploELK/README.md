# Ejemplo de reporte ELK

Este ejemplo integra una instalacion de ELK + Filebeat con un proyecto spring boot

# Detalle de los sistemas

Swagger Codegen 2.3.1 (OpenApi 2.0)
Java 8
Spring-Boot 1.5.11.RELEASE
Spock 1.0-groovy-2.4
Elasticsearch 5
Kibana 5
Logstash 5
Jmeter 5.1

# Compilación Ejecución API de test

Entrar a la carpeta ApiNotas y ejecutar

```bash
mvn clean package
```

Luego entrar al directorio target y ejecutar

```bash
java -jar .\api-notas-1.0.0.jar
```

Esto no habilitara las siguientes URL

Documentación de Swagger
http://127.0.0.1:8080/notas

# Compilación Ejecución API de test

Levantar JMeter y cargar la prueba que se encuentra en:

jmeter-client\NotasApi.jmx


# Docker de ELK + FileBeat

Para monitoriar los log del sistema este escrobe los logs en el directorio c:\logs

Este directorio se montara dentro del docker para Filebeat

Entrar al directorio \docker-elk-filebeat y lebantar el docker compose
```bash
\docker-compose up
```

[Docker ELK](https://github.com/deviantony/docker-elk)

Las URL disponibles son

Kibana
http://localhost:5601

Elasticsearch
http://localhost:9200
http://localhost:9300

Logstash
http://localhost:5000
http://localhost:5044


#Ejecutando la prueba

1.- Ejecutar API y generar log en C:\logs
2.- Ejecutar http://localhost:5601
3.- Crear Índice (esperar que índice se cree)
4.- Ir a discover y crear alguna búsqueda (por ejemplo "ERROR")
5.- Ir a visualize y crear una visualización de la búsqueda anterior
6.- Ir a Dashboard y agregar una visualización.
7.- Entrar en http://127.0.0.1:8080/notas y leer un dato id 1 (visualizar 1 error)
8.- Agregar un dato a API en http://127.0.0.1:8080/notas post nota
9.- Ejecutar jmeter y ver estadística


Ejemplos de Metrica

![Test Ejemplo 1](https://bytebucket.org/MigMMSoft/ejemplospringboot/raw/7c766afb3c1d64a760b44e4b42a845692e575554/EjemploELK/EjemploTestELK.png)
