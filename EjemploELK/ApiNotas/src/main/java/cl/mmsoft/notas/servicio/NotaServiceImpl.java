package cl.mmsoft.notas.servicio;

import cl.mmsoft.notas.domain.EntidadNota;
import cl.mmsoft.notas.swagger.codegen.model.Nota;
import java.io.Serializable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Sort;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
        
/**
 *
 * @author mig_s
 */
@Service
@Transactional(readOnly = true)
public class NotaServiceImpl implements Serializable{
    @Autowired
    private NotaRepository notaRepository;
    
    public Nota getNota(Long notaId){
        if(notaId != null){
            EntidadNota entidadNota = notaRepository.getOne(notaId);
            if(entidadNota != null){
                return getNotaSwagger(entidadNota);
            }
        }
        
        return null;
    }
    
    @Transactional
    public void saveNota(Nota nota){
        if(nota != null){
            EntidadNota entidadNota = new EntidadNota();
            entidadNota.setTitulo(nota.getTitulo());
            entidadNota.setComentario(nota.getComentario());
            notaRepository.saveAndFlush(entidadNota);
        }
    }
    
    @Transactional
    public void update(Nota nota){
        if(nota != null){
            EntidadNota entidadNota = notaRepository.getOne(nota.getId());
            if(entidadNota != null){
                entidadNota.setTitulo(nota.getTitulo());
                entidadNota.setComentario(nota.getComentario());
                notaRepository.saveAndFlush(entidadNota);
            }
        }
    }
    
    @Transactional
    public void delete(Long notaId){
        if(notaId != null){
            EntidadNota entidadNota = notaRepository.getOne(notaId);
            if(entidadNota != null){
                notaRepository.delete(notaId);
            }
        }
    }
    
    public List<Nota> getNotas(Integer offset, Integer max){
        Integer o = offset!=null?offset:0;
        Integer m = max!=null?max:0;
        Page<EntidadNota> entidadesNotas = notaRepository.findAllNotas(new PageRequest( o, m, Sort.Direction.DESC, "id" ));
        
        List<Nota> notas = new ArrayList();
        if(entidadesNotas != null){
            entidadesNotas.forEach(entidad->{
                notas.add(getNotaSwagger(entidad));
            });
        }
        
        return notas;
    }
    
    public Nota getNotaSwagger(EntidadNota entidadNota){
        Nota nota = new Nota();
        nota.setId(entidadNota.getId());
        nota.setTitulo(entidadNota.getTitulo());
        nota.setComentario(entidadNota.getComentario());
        
        if(entidadNota.getFechaCreacion() != null){
            nota.setFechaCreacion(OffsetDateTime.ofInstant(entidadNota.getFechaCreacion().toInstant(), ZoneId.systemDefault()));
        }
        if(entidadNota.getFechaActualizacion() != null){
           nota.setFechaActualizacion(OffsetDateTime.ofInstant(entidadNota.getFechaActualizacion().toInstant(), ZoneId.systemDefault()));
        }
        return nota;
    }
}

