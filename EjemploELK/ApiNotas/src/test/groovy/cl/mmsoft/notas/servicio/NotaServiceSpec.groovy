package cl.mmsoft.notas.servicio

import spock.lang.Specification
import spock.lang.Subject

import cl.mmsoft.notas.swagger.codegen.model.Nota
import java.time.OffsetDateTime
import cl.mmsoft.notas.swagger.Swagger2SpringBoot
import cl.mmsoft.notas.domain.EntidadNota

import org.springframework.test.context.junit4.SpringRunner
import org.springframework.boot.test.context.SpringBootTest

import org.springframework.data.domain.Page

/**
 *
 * @author mig_s
 */
@SpringBootTest(classes = Swagger2SpringBoot.class)
class NotaServiceTest extends Specification{
    
    @Subject NotaServiceImpl instance
    NotaRepository repo = Mock(NotaRepository.class)
    Nota notaTest
    EntidadNota notaEntidad
    
    
    def setup() {
        instance = new NotaServiceImpl()
        instance.notaRepository = repo
        notaTest = new Nota()
        notaEntidad = new EntidadNota()
    }
    
    def "rescata una nota parametro nulo"(){
        when:
            def nota = null
        then:
            instance.getNota(nota) == null
    }
    
    def "rescata una nota nula"(){
        given:
            repo.getOne( _ as Nota) >> {Nota notaId -> null} 
        when:
            def nota = 1L
        then:
            instance.getNota(nota) == null
    }
    
    def "rescata una nota"(){
        setup:
            NotaRepository repo = Mock(NotaRepository.class)
            instance.notaRepository = repo
            notaEntidad.with{
                id = 1L
                titulo = "Nota uno"
                comentario = "Prueba Nota Uno"
                fechaCreacion = new Date()
                fechaActualizacion = new Date()
            }
            1 * repo.getOne(1L) >> notaEntidad

        when:
            def nota = 1L
        then:
            instance.getNota(nota) != null
    }
    
    def "Graba una nota nula"(){
        when:
            def nota = null
        then:
            instance.saveNota(nota)
    }
    
    def "Graba una nueva nota"(){
        when:
            notaTest.with{
                id = 1L
                titulo = "Nota uno"
                comentario = "Prueba Nota Uno"
                fechaCreacion = OffsetDateTime.now()
                fechaActualizacion = OffsetDateTime.now()
            }
        then:
            instance.saveNota(notaTest)
    }
    
    def "actualiza una nota nula"(){
        when:
            def nota = null
        then:
            instance.update(nota)
    }
    
    def "actualiza una nota con id nula"(){
        when:
            notaTest.with{
                id = 1L
                titulo = "Nota uno"
                comentario = "Prueba Nota Uno"
                fechaCreacion = OffsetDateTime.now()
                fechaActualizacion = OffsetDateTime.now()
            }
        then:
            instance.update(notaTest)
    }

    def "actualiza una nota"(){
        when:
            notaTest.with{
                id = 1L
                titulo = "Nota uno"
                comentario = "Prueba Nota Uno"
                fechaCreacion = OffsetDateTime.now()
                fechaActualizacion = OffsetDateTime.now()
            }
        then:
            instance.update(notaTest)
    }
    
    def "elimina una nota nula"(){
        when:
            def nota = null
        then:
            instance.delete(nota)
    }
    
    def "elimina una nota"(){
        when:
            def nota = 1L
        then:
            instance.delete(nota)
    }
    
    def "retorno lista Nula"(){
        setup: 
            repo.findAllNotas( _ as Page<EntidadNota>) >> { Page<EntidadNota> notas ->  null} 
    
        when:
            Integer offset = 0 
            Integer max = 10
            
        then:
            instance.getNotas(offset, max)==[]
    }
    
    def "retorno lista vacia"(){
        setup:
            repo.findAllNotas( _ as Page<EntidadNota>) >> { Page<EntidadNota> notas ->  []} 
    
        when:
            Integer offset = 0 
            Integer max = 10
            
        then:
            instance.getNotas(offset, max)==[]
    }
}

