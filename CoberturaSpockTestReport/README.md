# Ejemplo de reporte test mojo y spock

Ejemplo de generación de reporte de cobertura con mojo y spock test

# Detalle de los sistemas

Swagger Codegen 2.3.1 (OpenApi 2.0)
Java 8
Spring-Boot 1.5.11.RELEASE
Spock 1.0-groovy-2.4

# Compilación Ejecución
Para generar el reporte se debe ejecutar el siguiente comando

```bash
mvn cobertura:cobertura
```

Entrar a la carpeta ApiNotas y ejecutar

```bash
mvn clean package
```

Luego entrar al directorio target y ejecutar

```bash
java -jar .\api-notas-1.0.0.jar
```

Esto no habilitara las siguientes URL

Documentación de Swagger
http://127.0.0.1:8080/notas

Mojo reporte cobertura
/target/site/cobertura/index.html

Ejemplos de cobertura test usando Spock Test

![Test Ejemplo 1](https://bitbucket.org/MigMMSoft/ejemplospringboot/src/master/MojoSpockTestReport/EjemploTestSpock.png)
