package cl.mmsoft.notas.swagger.codegen.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import cl.mmsoft.notas.servicio.NotaServiceImpl;
import cl.mmsoft.notas.swagger.codegen.model.Nota;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class NotasApiController implements NotasApi {
    private static final Logger LOG = LoggerFactory.getLogger(NotasApiController.class);
    
    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public NotasApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }
    
    @Autowired
    private NotaServiceImpl notaServiceImpl;

    @Override
    public ResponseEntity<List<Nota>> verNotas(@ApiParam(value = "pagina a rescatar (empieza en 0)") @Valid @RequestParam(value = "offset", required = false) Integer offset,@ApiParam(value = "elementos a rescatar (por defecto 10)") @Valid @RequestParam(value = "max", required = false) Integer max) {
        try {
            return new ResponseEntity<List<Nota>>(notaServiceImpl.getNotas(offset, max), HttpStatus.OK);
        } catch (Exception e) {
            LOG.error("ERROR: verNotas", e);
            return new ResponseEntity<List<Nota>>(HttpStatus.BAD_REQUEST);
        }
    }
}
