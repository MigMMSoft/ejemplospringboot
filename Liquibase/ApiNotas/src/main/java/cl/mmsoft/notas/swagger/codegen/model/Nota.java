package cl.mmsoft.notas.swagger.codegen.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.time.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Nota
 */
@Validated

public class Nota   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("titulo")
  private String titulo = null;

  @JsonProperty("comentario")
  private String comentario = null;

  @JsonProperty("fechaCreacion")
  private OffsetDateTime fechaCreacion = null;

  @JsonProperty("fechaActualizacion")
  private OffsetDateTime fechaActualizacion = null;

  public Nota id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Nota titulo(String titulo) {
    this.titulo = titulo;
    return this;
  }

  /**
   * Get titulo
   * @return titulo
  **/
  @ApiModelProperty(value = "")

@Size(min=2,max=100) 
  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public Nota comentario(String comentario) {
    this.comentario = comentario;
    return this;
  }

  /**
   * Get comentario
   * @return comentario
  **/
  @ApiModelProperty(value = "")

@Size(min=2,max=100) 
  public String getComentario() {
    return comentario;
  }

  public void setComentario(String comentario) {
    this.comentario = comentario;
  }

  public Nota fechaCreacion(OffsetDateTime fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
    return this;
  }

  /**
   * Get fechaCreacion
   * @return fechaCreacion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getFechaCreacion() {
    return fechaCreacion;
  }

  public void setFechaCreacion(OffsetDateTime fechaCreacion) {
    this.fechaCreacion = fechaCreacion;
  }

  public Nota fechaActualizacion(OffsetDateTime fechaActualizacion) {
    this.fechaActualizacion = fechaActualizacion;
    return this;
  }

  /**
   * Get fechaActualizacion
   * @return fechaActualizacion
  **/
  @ApiModelProperty(value = "")

  @Valid

  public OffsetDateTime getFechaActualizacion() {
    return fechaActualizacion;
  }

  public void setFechaActualizacion(OffsetDateTime fechaActualizacion) {
    this.fechaActualizacion = fechaActualizacion;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Nota nota = (Nota) o;
    return Objects.equals(this.id, nota.id) &&
        Objects.equals(this.titulo, nota.titulo) &&
        Objects.equals(this.comentario, nota.comentario) &&
        Objects.equals(this.fechaCreacion, nota.fechaCreacion) &&
        Objects.equals(this.fechaActualizacion, nota.fechaActualizacion);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, titulo, comentario, fechaCreacion, fechaActualizacion);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Nota {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    titulo: ").append(toIndentedString(titulo)).append("\n");
    sb.append("    comentario: ").append(toIndentedString(comentario)).append("\n");
    sb.append("    fechaCreacion: ").append(toIndentedString(fechaCreacion)).append("\n");
    sb.append("    fechaActualizacion: ").append(toIndentedString(fechaActualizacion)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

