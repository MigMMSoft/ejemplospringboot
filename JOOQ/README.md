# Ejemplo Liquibase

Ejemplo de generación con Swagger -> StringBoot
Más DevTools de Spring
Más versionador de base de datos Liquibase

# Detalle de los sistemas

Swagger Codegen 2.3.1 (OpenApi 2.0)
Java 8
Spring-Boot 1.5.11.RELEASE
Liquibase

# Compilación Ejecución

Entrar a la carpeta ApiNotas y ejecutar

```bash
mvn clean package
```

Luego entrar al directorio target y ejecutar

```bash
java -jar .\api-notas-1.0.0.jar
```

Esto no habilitara las siguientes URL

Documentación de Swagger
http://127.0.0.1:8080/notas

Spring DevTool
http://127.0.0.1:8080/notas/h2-console

# Recrear o agregar funcionalidad

En caso que querer agregar nuevos métodos a la documentación de Swagger o modificar el documento existente, los pasos son los siguientes:

Bajar SwaggerCodeGen 2.3.1

```bash
wget https://oss.sonatype.org/content/repositories/releases/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
```

La configuración de creación de clases con swaggerCodeGen se encuentra en swagger/config.json

Para recrear el proyecto se debe ejecutar la siguiente instrucción (parado en raíz del repositorio):

```bash
 java -jar .\swagger-codegen-cli.jar generate -i .\swagger\api_notas.yaml -l spring -c .\swagger\config.json -o ApiNotas --ignore-file-override .\.swagger-codegen-ignore
```

En este proyecto se encuentra en el archivo ubicado en ApiNotas\.swagger-codegen-ignore, todos las clases que no se sobrescribirán con la instrucción de creación de clases de swaggerCodeGen. Si necesitamos proteger algún otro archivo que no deseamos que se vuelva a generar se debe agregar a este archivo

```bash
pom.xml
src/main/java/cl/mmsoft/notas/swagger/codegen/api/NotaApiController.java
src/main/java/cl/mmsoft/notas/swagger/codegen/api/NotasApiController.java
src/main/java/cl/mmsoft/notas/swagger/codegen/api/VersionApiController.java
src/main/java/cl/mmsoft/notas/swagger/Swagger2SpringBoot.java
src/main/resources/application.properties
```