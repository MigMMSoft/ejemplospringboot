--V3__create_book_table_and_records.sql
INSERT INTO hooqTest.NOTA
(FECHA_ACTUALIZACION, FECHA_CREACION, COMENTARIO, TITULO)
VALUES(GETDATE(), GETDATE(), 'Esta es la primera nota en insert', 'Nota 1');

INSERT INTO hooqTest.NOTA
(FECHA_ACTUALIZACION, FECHA_CREACION, COMENTARIO, TITULO)
VALUES(GETDATE(), GETDATE(), 'Esta es la segunda nota en insert', 'Nota 2');

INSERT INTO hooqTest.NOTA
(FECHA_ACTUALIZACION, FECHA_CREACION, COMENTARIO, TITULO)
VALUES(GETDATE(), GETDATE(), 'Esta es la tecera nota en insert', 'Nota 3');

INSERT INTO hooqTest.NOTA
(FECHA_ACTUALIZACION, FECHA_CREACION, COMENTARIO, TITULO)
VALUES(GETDATE(), GETDATE(), 'Esta es la cuarta nota en insert', 'Nota 4');