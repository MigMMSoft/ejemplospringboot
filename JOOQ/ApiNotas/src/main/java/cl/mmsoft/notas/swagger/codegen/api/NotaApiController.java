package cl.mmsoft.notas.swagger.codegen.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Controller;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import cl.mmsoft.notas.servicio.NotaServiceImpl;
import cl.mmsoft.notas.swagger.codegen.model.Nota;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 
 * @author mig_s
 */
@Controller
public class NotaApiController implements NotaApi {
    private static final Logger LOG = LoggerFactory.getLogger(NotaApiController.class);
    
    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public NotaApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    @Override
    public Optional<ObjectMapper> getObjectMapper() {
        return Optional.ofNullable(objectMapper);
    }

    @Override
    public Optional<HttpServletRequest> getRequest() {
        return Optional.ofNullable(request);
    }

    @Autowired
    private NotaServiceImpl notaServiceImpl;

    /**
     * 
     * @param body
     * @return 
     */
    @Override
    public ResponseEntity<Void> actualizarNota(
            @ApiParam(value = "Nota que va a ser actualizado en el sistema", required = true) @RequestBody Nota body
    ) {
        try {
            notaServiceImpl.update(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            LOG.error("ERROR: actualizarNota", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public ResponseEntity<Void> eliminarNota(@ApiParam(value = "Id del Nota a eliminar", required = true) @RequestParam(value = "id", required = true) Long id
    ) {
        try {
            notaServiceImpl.delete(id);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            LOG.error("ERROR: eliminarNota", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * 
     * @param body
     * @return 
     */
    @Override
    public ResponseEntity<Void> ingresarNota(
            @ApiParam(value = "Nota que va a ser agregada al sistema", required = true) @RequestBody Nota body
    ) {
        try {
            notaServiceImpl.saveNota(body);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch (Exception e) {
            LOG.error("ERROR: ingresarNota", e);
            return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
        }
    }
    
    /**
     * 
     * @param id
     * @return 
     */
    @Override
    public ResponseEntity<Nota> verNota(@ApiParam(value = "Id del Nota a eliminar", required = true) @RequestParam(value = "id", required = true) Long id
    ) {
        try {
            Nota nota = notaServiceImpl.getNota(id);
            return new ResponseEntity<Nota>(nota, HttpStatus.OK);
        } catch (Exception e) {
            LOG.error("ERROR: verNota", e);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
