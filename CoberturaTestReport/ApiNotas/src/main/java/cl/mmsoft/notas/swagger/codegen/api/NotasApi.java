/**
 * NOTE: This class is auto generated by the swagger code generator program (2.3.1).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package cl.mmsoft.notas.swagger.codegen.api;

import cl.mmsoft.notas.swagger.codegen.model.Nota;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Api(value = "notas", description = "the notas API")
public interface NotasApi {

    Logger log = LoggerFactory.getLogger(NotasApi.class);

    default Optional<ObjectMapper> getObjectMapper() {
        return Optional.empty();
    }

    default Optional<HttpServletRequest> getRequest() {
        return Optional.empty();
    }

    default Optional<String> getAcceptHeader() {
        return getRequest().map(r -> r.getHeader("Accept"));
    }

    @ApiOperation(value = "Encuentra el detalle de todas las Notas", nickname = "verNotas", notes = "", response = Nota.class, responseContainer = "List", tags={ "Notas", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Notas validas", response = Nota.class, responseContainer = "List"),
        @ApiResponse(code = 400, message = "Los datos ingresados no son validos") })
    @RequestMapping(value = "/notas",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    default ResponseEntity<List<Nota>> verNotas(@ApiParam(value = "pagina a rescatar (empieza en 0)") @Valid @RequestParam(value = "offset", required = false) Integer offset,@ApiParam(value = "elementos a rescatar (por defecto 10)") @Valid @RequestParam(value = "max", required = false) Integer max) {
        if(getObjectMapper().isPresent() && getAcceptHeader().isPresent()) {
            if (getAcceptHeader().get().contains("application/json")) {
                try {
                    return new ResponseEntity<>(getObjectMapper().get().readValue("[ {  \"comentario\" : \"comentario\",  \"titulo\" : \"titulo\",  \"fechaCreacion\" : \"2000-01-23T04:56:07.000+00:00\",  \"fechaActualizacion\" : \"2000-01-23T04:56:07.000+00:00\",  \"id\" : 0}, {  \"comentario\" : \"comentario\",  \"titulo\" : \"titulo\",  \"fechaCreacion\" : \"2000-01-23T04:56:07.000+00:00\",  \"fechaActualizacion\" : \"2000-01-23T04:56:07.000+00:00\",  \"id\" : 0} ]", List.class), HttpStatus.NOT_IMPLEMENTED);
                } catch (IOException e) {
                    log.error("Couldn't serialize response for content type application/json", e);
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        } else {
            log.warn("ObjectMapper or HttpServletRequest not configured in default NotasApi interface so no example is generated");
        }
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

}
