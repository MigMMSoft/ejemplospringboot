package cl.mmsoft.notas.servicio;

import cl.mmsoft.notas.swagger.Swagger2SpringBoot;
import cl.mmsoft.notas.swagger.codegen.model.Nota;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;

/**
 *
 * @author mig_s
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes={Swagger2SpringBoot.class})
public class NotaServiceImplTest {

    @Autowired
    private NotaServiceImpl instance;

    /**
     * Test of getNota method, of class NotaServiceImpl.
     */
    @Test
    public void testGetNota() {
        // given
        Nota nota = new Nota();
        nota.setTitulo("test");
        nota.setComentario("Nota test");
        
        // when
        List<Nota> notasOld = instance.getNotas(0, 10);
        instance.saveNota(nota);

        // then
        List<Nota> notas = instance.getNotas(0, 10);
        assertNotNull(notas);
        assertFalse("Lista no está vacía", notas.isEmpty());
        assertNotEquals(notasOld.size(), notas.size());
    }
    
    @Test
    public void testUpdate() {
        // given
        Nota nota = new Nota();
        nota.setTitulo("test");
        nota.setComentario("Nota test");
        
        // when
        instance.saveNota(nota);
        
        nota.setId(1L);
        nota.setTitulo("test 2");
        nota.setComentario("Nota test 2");
        
        instance.update(nota);
        
        // then
        Nota notaUpdate = instance.getNota(1L);
        
        assertNotNull(notaUpdate);
        assertEquals(notaUpdate.getTitulo(), "test 2");
        assertEquals(notaUpdate.getComentario(), "Nota test 2");
    }
    
    @Test
    public void testDeleteNota() {
        // given
        Nota nota = new Nota();
        nota.setTitulo("test");
        nota.setComentario("Nota test");
        
        // when
        instance.saveNota(nota);
        List<Nota> notasOld = instance.getNotas(0, 10);

        // then
        Nota notaDel = notasOld.get(0);
        instance.delete(notaDel.getId());
        List<Nota> notas = instance.getNotas(0, 10);
        assertNotNull(notas);
        assertFalse("Lista no está vacía", notasOld.isEmpty());
        assertNotEquals(notasOld.size(), notas.size());
    }
}
