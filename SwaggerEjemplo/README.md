# Ejemplo Swagger

Ejemplo de generación con Swagger -> StringBoot.
Con este ejemplo se genera la base de un servidor REST a partir de la definición del API en el  formato YAML para generar el SpringBoot. Para el ejemplo se requiere acceso a internet para bajar el Swagger Code Gen que es el encargado de interpretar la definición de api escrita en el YAML

# Requerimientos mínimos

Java 8
Maven 3
Docker *no es obligatorio el uso de docker*

# Detalle de los sistemas
Swagger Codegen 2.3.1 (OpenApi 2.0)
Java 8
Spring-Boot 1.5.11.RELEASE
Spring-Cloud Edgware.SR3

# Preparacion

Bajar SwaggerCodeGen 2.3.1

```bash
wget https://oss.sonatype.org/content/repositories/releases/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
```

Para editar la configuración se puede hacer de dos formas, online o local usando docket:

Para uso local con docker:
```bash
docker pull swaggerapi/swagger-editor
docker run -d -p 80:8080 swaggerapi/swagger-editor

Swagger editor quedara disponible en la siguiente dirección
http://127.0.0.1
```
Para uso online usar:

https://editor.swagger.io

Nota: Usar la versión online solo para las pruebas, ya que el editor online deja las definiciones de API's online compartida.

Para genera la aplicación utilizando el codegen se debe ejecutar la siguiente instrucción este generara un proyecto maven con springboot llamado ApiNotas:

```bash
 java -jar .\swagger-codegen-cli.jar generate -i .\swagger\api_notas.yaml -l spring -o ApiNotas
```

Para generar el API customisando PATH, lenguje y framework

```bash
 java -jar .\swagger-codegen-cli.jar generate -i .\swagger\api_notas.yaml -l spring -c .\swagger\config.json -o ApiNotas --ignore-file-override .\.swagger-codegen-ignore
```

Una vez ejecutada la instrucción de generación de la API se debe compilar. Se debe ingresar al directorio \ApiNotas y luego ejecutar la instrucción a continuación.

```bash
mvn clean package
```

Una vez compilada la API para levantar y consumir el API se debe ejecutar la siguiente instrucción:

```bash
java -jar .\target\swagger-spring-1.0.0.jar
```

Una levantada la API queda disponible una dirección local para consumir el API más la documentación de Swagger:

http://127.0.0.1:8080/notas