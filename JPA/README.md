# Ejemplo DevTools

Ejemplo de generación con Swagger -> StringBoot
Más DevTools de Spring
Más versionador de base de datos utilizando JPA

Para este ejemplo se preparó una base SQLServer en docker, Ya que la base H2 no tiene no se comporta correctamente con esta implementación

# Detalle de los sistemas

Swagger Codegen 2.3.1 (OpenApi 2.0)
Java 8
Spring-Boot 1.5.11.RELEASE
sqlserver

# Compilación Ejecución

Preparación de la base de datos

```bash
docker pull mcr.microsoft.com/mssql/server:2017-latest

docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Desarrollo#" ` -p 1433:1433 --name sql1 ` -d mcr.microsoft.com/mssql/server:2017-latest
```
Una vez arriba la base de datos se debe crear la base y el usuario de la aplicacion

Crea la Base de datos:
```sql
CREATE DATABASE [API_NOTAS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'API_NOTAS', FILENAME = N'/var/opt/mssql/data/API_NOTAS.mdf' , SIZE = 8192KB , FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'API_NOTAS_log', FILENAME = N'/var/opt/mssql/data/API_NOTAS_log.ldf' , SIZE = 8192KB , FILEGROWTH = 65536KB );
```

Crea el usuario de base de datos:
```sql
USE [API_NOTAS];
CREATE LOGIN [APP_API_NOTAS] WITH PASSWORD=N'Desarrollo', DEFAULT_DATABASE=[API_NOTAS], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
CREATE USER [APP_API_NOTAS] FOR LOGIN [APP_API_NOTAS];
ALTER LOGIN [APP_API_NOTAS] ENABLE;
ALTER ROLE [db_ddladmin] ADD MEMBER [APP_API_NOTAS];
ALTER ROLE [db_datawriter] ADD MEMBER [APP_API_NOTAS];
ALTER ROLE [db_datareader] ADD MEMBER [APP_API_NOTAS];
```


Una vez creada la base de datos y usuario, compilar la aplicación.
```bash
mvn clean package
```

Luego entrar al directorio target y ejecutar

```bash
java -jar .\api-notas-1.0.0.jar
```

Esto no habilitara las siguientes URL

Documentación de Swagger
http://127.0.0.1:8080/notas


# Preparacion

Bajar SwaggerCodeGen 2.3.1

```bash
wget https://oss.sonatype.org/content/repositories/releases/io/swagger/swagger-codegen-cli/2.3.1/swagger-codegen-cli-2.3.1.jar -O swagger-codegen-cli.jar
```

Configuración de swaggerCodeGen en swagger/config.json

Crear servicio Rest y documentación a través de jar swaggerCodeGen (parado en raíz del repositorio):

```bash
 java -jar .\swagger-codegen-cli.jar generate -i .\swagger\api_notas.yaml -l spring -c .\swagger\config.json -o ApiNotas --ignore-file-override .\.swagger-codegen-ignore
```

Una vez generado el código agregar al ApiNotas\.swagger-codegen-ignore el archivo .xml y controladores que no deberían volver a auto generarse

```bash
pom.xml
src/main/java/cl/mmsoft/notas/swagger/codegen/api/NotaApiController.java
src/main/java/cl/mmsoft/notas/swagger/codegen/api/NotasApiController.java
src/main/java/cl/mmsoft/notas/swagger/codegen/api/VersionApiController.java
```

Agregar DevTools a pom.xml

```xml
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-devtools</artifactId>
	<scope>runtime</scope>
</dependency>
```

