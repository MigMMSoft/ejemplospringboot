package cl.mmsoft.notas.domain

import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.Column
import javax.persistence.Entity

/**
 *
 * @author mgonzalez@mmsoft.cl
 */
@Entity
@Table(name = "nota")
class EntidadNota extends AbstractEntity<String>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id
    
    @Column(name = "titulo", length = 100)
    String titulo
    
    @Column(name = "comentario", length = 100)
    String comentario	
}

